# bt2dpy

find the eigenvalues / eigenvectors of a Bloch Torrey operator on a specific geometry using
finite elements method. Sparse matrices representing the problem are calculated with matlab's
PDETool which are then diagonalised using this python program to allow calculations on big clusters.