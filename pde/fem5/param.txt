Exported FEM matrices and mesh data
-------------------
Parameters
L=0.500000
H=0.500000
angle_g=1.178097
a=0.300000
(arrondis=1)
mesh_normal=0.010000
(mesh_coins=0.010000)
-------------------
FEM infos
length_p=11137
size of FEM matrices=11137 x 11137
non zero elements in A=77249
non zero elements in B=77249
non zero elements in M=77249
Sparse matrices file format (A, B, M) : row,col,value
export time=7.731669 s
