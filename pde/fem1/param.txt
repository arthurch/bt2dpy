Exported FEM matrices and mesh data
-------------------
Parameters
L=1.000000
H=0.707107
angle_g=0.785398
a=0.200000
(arrondis=1)
mesh_normal=0.010000
(mesh_coins=0.010000)
-------------------
FEM infos
length_p=32440
size of FEM matrices=32440 x 32440
non zero elements in A=225774
non zero elements in B=225774
non zero elements in M=225774
export time=14.814902 s
