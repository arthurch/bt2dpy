Exported FEM matrices and mesh data
-------------------
Parameters
L=1.000000
H=0.707107
angle_g=0.785398
a=0.700000
(arrondis=1)
mesh_normal=0.010000
(mesh_coins=0.010000)
-------------------
FEM infos
length_p=28655
size of FEM matrices=28655 x 28655
non zero elements in A=199451
non zero elements in B=199451
non zero elements in M=199451
Sparse matrices file format (A, B, M) : row,col,value
export time=13.853502 s
