Exported FEM matrices and mesh data
-------------------
Parameters
L=1.000000
H=0.707107
angle_g=0.785398
a=0.500000
(arrondis=1)
mesh_normal=0.010000
(mesh_coins=0.010000)
-------------------
FEM infos
length_p=30264
size of FEM matrices=30264 x 30264
non zero elements in A=210654
non zero elements in B=210654
non zero elements in M=210654
Sparse matrices file format (A, B, M) : row,col,value
export time=14.039736 s
