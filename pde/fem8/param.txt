Exported FEM matrices and mesh data
-------------------
Parameters
L=0.500000
H=0.500000
angle_g=1.178097
a=0.200000
(arrondis=1)
mesh_normal=0.005000
(mesh_coins=0.010000)
-------------------
FEM infos
length_p=42700
size of FEM matrices=42700 x 42700
non zero elements in A=297662
non zero elements in B=297662
non zero elements in M=297662
Sparse matrices file format (A, B, M) : row,col,value
export time=18.513104 s
