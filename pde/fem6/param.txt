Exported FEM matrices and mesh data
-------------------
Parameters
L=0.500000
H=0.500000
angle_g=1.178097
a=0.400000
(arrondis=1)
mesh_normal=0.010000
(mesh_coins=0.010000)
-------------------
FEM infos
length_p=10383
size of FEM matrices=10383 x 10383
non zero elements in A=72019
non zero elements in B=72019
non zero elements in M=72019
Sparse matrices file format (A, B, M) : row,col,value
export time=7.421709 s
