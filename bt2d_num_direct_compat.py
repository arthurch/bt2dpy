from scipy import sparse as sp
from scipy.sparse import linalg as splinalg
from scipy import io as scio
import numpy as np
import time
import datetime
import os
import sys

import logging
logger = logging.getLogger(__name__)

# functions
def import_sparse(filename, M, N, nnz=0):
    if(nnz == 0):
        with open(filename, "r") as f:
            row_ind = []
            col_ind = []
            data = []
            for l in f:
                s = l.split(",")
                row_ind.append(int(s[0])-1)
                col_ind.append(int(s[1])-1)
                data.append(float(s[2]))
            mat = sp.csr_matrix((data, (row_ind, col_ind)), [M, N])
            return mat
    else:
        with open(filename, "r") as f:
            row_ind = np.ones(nnz)
            col_ind = np.ones(nnz)
            data = np.ones(nnz)
            i = 0
            for l in f:
                s = l.split(",")
                row_ind[i] = int(s[0])-1
                col_ind[i] = int(s[1])-1
                data[i] = float(s[2])
                i += 1
            mat = sp.csr_matrix((data, (row_ind, col_ind)), [M, N])
            return mat

def main():
    if(len(sys.argv)>1):
        print("cmd arguments :", sys.argv[1:])
        fem_folder = 'pde/' + sys.argv[1] + '/'
    else:
        fem_folder = "pde/fem2/"
    print("fem folder is "+fem_folder)
    logger.info("fem folder is "+fem_folder)

    # mat_size = 32440 # fem1
    mat_size = 1329 # fem2

    # find size of fem matrices
    with open(fem_folder + "param.txt") as params_f:
        for line in params_f:
            if("length_p" in line):
                mat_size = int(line.split("=")[1])
                print("using fem matrices of size", mat_size)
                logger.info("using fem matrices of size %d", mat_size)

    row = 1
    out_folder = fem_folder + "calc"+str(row)
    while(os.path.isdir(out_folder) and row<1000):
        row = row+1
        out_folder = fem_folder+"calc"+str(row)
    os.mkdir(out_folder)
    print("out to :", out_folder)
    logger.info("out to :", out_folder)

    logging.basicConfig(filename=out_folder + '/calc.log', level=logging.DEBUG)
    logger.info('Started calculations')

    start = time.time()

    # load data
    A = import_sparse(fem_folder+"A.csv", mat_size, mat_size)
    B = import_sparse(fem_folder+"B.csv", mat_size, mat_size)
    M = import_sparse(fem_folder+"M.csv", mat_size, mat_size)

    end = time.time()
    print("Elapsed time", end - start, "s")
    logger.info("Elapsed time %f s", end - start)

    # find eigenvalues
    # gs = np.round(np.linspace(0, 50000, 500), -1)
    gs = np.array([20000])
    nLambda = 150

    nev = min(30, len(gs))
    step_ev = np.max([len(gs)//nev, 1])

    lambdas = np.zeros((len(gs), nLambda), dtype=np.cdouble)
    gs_ev = np.zeros(nev)
    Vs = np.zeros((nev, nLambda, mat_size), dtype=np.cdouble)

    errors = []

    for n in range(len(gs)):
        # sparse calculation
        # mat1'.V' = mat2'.V'.D
        mat1 = A + 1j*gs[n]*B
        mat1 = mat1.transpose()
        mat2 = M.transpose()
        try:
            lambdasG, Vtilde = sp.linalg.eigs(mat1, nLambda, M=mat2, which="SR", tol=10e-5)
            # lambdasG, Vtilde = splinalg.eigs(mat1, nLambda, M=mat2, sigma=0, which="LM", tol=10e-5)
        except sp.linalg.ArpackNoConvergence as e:
            lambdasG = np.zeros(nLambda, dtype=np.cdouble)
            lambdasG[:len(e.eigenvalues)] = e.eigenvalues
            Vtilde = np.zeros((nLambda, mat_size), dtype=np.cdouble)
            Vtilde[:e.eigenvectors.shape[0], :] = e.eigenvectors
            print("ERREUR: only", len(lambdasG), "eigenvalues converged")
            print(e)
            logger.warn("ERREUR: only %d eigenvalues converged", len(lambdasG))
            logger.warn(str(e))
            errors.append(str(e))
        """
        v = An array of k eigenvectors. v[:, i] is the eigenvector corresponding to the eigenvalue w[i].
        """

        I = np.argsort(np.real(lambdasG))
        lambdasG = lambdasG[I]
        lambdas[n, :] = lambdasG

        # trie des vps par les parties imaginaires 
        for m in range(len(lambdasG)-1):
            delta = np.abs(np.real(lambdasG[m]) - np.real(lambdasG[m+1]))
            if(delta < 10^-2):
                if(np.imag(lambdasG[m]) < np.imag(lambdasG[m+1])): # tri : Im(lambda) > 0 puis Im(lambda) < 0
                    # on doit inverser les vp
                    temp = lambdasG[m]
                    lambdasG[m] = lambdasG[m+1]
                    lambdasG[m+1] = temp

                    temp = I[m]
                    I[m] = I[m+1]
                    I[m+1] = temp

        if(n%step_ev == 0):
            # processing the eingenvectors
            end = time.time()
            print("done ", n, "/", len(gs), " (el time=", end-start, " s)", sep="")
            logger.info("done %d/%d (el time=%f s)", n, len(gs), end-start)
            V = Vtilde.transpose()
            V = V[I, :]
            Vs[n//step_ev, :, :] = V
            gs_ev[n//step_ev] = gs[n]
            # normalize the eigenvectors ?

    end = time.time()
    print("Elapsed time", end - start, "s")
    logger.info("Elapsed time %f s", end - start)

    # print(lambdas)

    # export results

    f = open(out_folder + '/infos.txt', 'w')
    f.write("Calculation results "+str(row)+"\n")
    f.write("Started at : " + time.ctime(start)+"\n")
    f.write("Finished at : " + time.ctime(end)+"\n")
    f.write("Duration : " + str(datetime.timedelta(seconds=(end-start)))+"\n")
    f.write("mat_size="+str(mat_size)+"\n")
    f.write("nLambda="+str(nLambda)+"\n")
    f.write("len_gq="+str(len(gs))+"\n")
    f.write("len_gs_ev="+str(len(gs_ev))+"\n")
    if(len(errors) > 0):
        f.write("Errors : \n")
        for errmsg in errors:
            f.write("\t"+errmsg+"\n")
    f.write("g values :\n")
    f.write(str(gs) + "\n")
    f.write("g values for eigenvectors :\n")
    f.write(str(gs_ev) + '\n')
    f.close()

    scio.savemat(out_folder + '/lambdas.mat', {'gs':gs, 'lambdas_sv':lambdas})
    scio.savemat(out_folder + '/Vs.mat', {'gs_ev':gs_ev, 'Vs':Vs, 'normalized':0})

main()

"""
### Original Matlab code :

function zmoy = fct_zmoy_normalize(V, p, triangle, l)
    % zs = [pderesults.Eigenvectors(triangle(1), l), pderesults.Eigenvectors(triangle(2), l), ...
    %     pderesults.Eigenvectors(triangle(3), l)];
    % zmoy = mean(zs);
    zs = [V(l, triangle(1))*V(l, triangle(1)), ...
        V(l, triangle(2))*V(l, triangle(2)), ...
        V(l, triangle(3))*V(l, triangle(3))];
    zmoy = mean(zs);
end

lambdas = zeros(length(gs), nLambda);
Vs = zeros(length(gs), length(ls), length(p));

for n=1:length(gs)
    % % full vp calculation
    % % W'*A = D*W'*B
    % [X,LambdaG,Vtilde] = eig(A + 1i*gs(n)*B, M);
    % V = Vtilde';
    % [dR, I] = sort(real(diag(LambdaG)));
    % d1 = diag(LambdaG);
    % d1 = d1(I);
    % d1 = d1(1:45)

    % sparse calculation
    % A.V = B.V.D
    [Vtilde, LambdaG, flag] = eigs((A + 1i*gs(n)*B).', M.', nLambda, ...
        'smallestreal', 'Tolerance', 10^-5);
    V = Vtilde.';

    [LambdaGReal, I] = sort(real(diag(LambdaG)));
    Lambda0 = diag(LambdaG);
    Lambda0 = Lambda0(I);

    if(flag ~= 0)
        nanls = zeros(0);
        for m=1:length(Lambda0)
            if(isnan(Lambda0(m)))
                nanls(end+1) = m;
            end
        end
        disp("ERREUR: flag not 0, "+length(nanls)+" eigenvalues did not converge. (flag="+flag ...
            +", nanls="+num2str(nanls)+")");
    end

    % d2 = Lambda0(1:30)

    % trie des vps par les parties imaginaires 
    for m=1:length(Lambda0)-1
        delta = abs(real(Lambda0(m)) - real(Lambda0(m+1)));
        if(delta < 10^-2)
            if(imag(Lambda0(m)) < imag(Lambda0(m+1))) % tri : Im(lambda) > 0 puis Im(lambda) < 0
                % on doit inverser les vp
                temp = Lambda0(m);
                Lambda0(m) = Lambda0(m+1);
                Lambda0(m+1) = temp;
    
                temp = I(m);
                I(m) = I(m+1);
                I(m+1) = temp;
            end
        end
    end

    V = V(I, :);
    
    % on normalise la base
    toc

    if(normalize)
        for l=1:nLambda
            norm_cst = sqrt(pde_integral_direct(V, p, t, ...
                @(r, p, triangle) fct_zmoy_normalize(r, p, triangle, l)));
            if(norm_cst < 10^-5)
                disp("Erreur : impossible de normaliser la vp l=" + l + ", norm_cst="+norm_cst);
            else
                V(l, :) = V(l, :) ./ norm_cst;
            end
        end
    end

    % on récupère les vect p et les val p

    for m=1:length(ls)
        Vs(n, m, :) = V(ls(m), :);
    end
    lambdas(n, :) = Lambda0(1:nLambda);
end

"""
